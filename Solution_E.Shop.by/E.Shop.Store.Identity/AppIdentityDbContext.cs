﻿using E.Shop.Store.Identity.Models;
using Microsoft.AspNet.Identity.EntityFramework;

namespace E.Shop.Store.Identity
{
    public class AppIdentityDbContext : IdentityDbContext<AppUser>
    {
        public AppIdentityDbContext() : base("IdentityDb") { }

        public static AppIdentityDbContext Create()
        {
            return new AppIdentityDbContext();
        }
    }
}
