﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;

namespace E.Shop.Store.Identity.Models
{
   public class AppUser : IdentityUser
    {
        public DateTime Data { get; set; }
        public bool ConfirmBan { get; set; }
        public int IsBanHours { get; set; }
        public AppUser()
        {
        }
    }
}
