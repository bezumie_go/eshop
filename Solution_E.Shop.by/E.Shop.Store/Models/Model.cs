﻿namespace E.Shop.Store.Data.Models
{
    public class Model
    {
        public int ModelId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public short Warranty { get; set; }
        public int? ImgId { get; set; }

        public int CategoryId { get; set; }
        public short? AvailabilityId { get; set; }
        public short? DeliveryId { get; set; }

        public virtual Category Category { get; set; }
        public virtual Delivery Delivery { get; set; }
        public virtual Availability Availability { get; set; }
    }
}
