﻿using System.Collections.Generic;

namespace E.Shop.Store.Data.Models
{    
    public class Delivery
    {
        public short DeliveryId { get; set; }
        public string DeliveryType { get; set; }

        public virtual ICollection<Model> Models { get; set; }
    }
}
