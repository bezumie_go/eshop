﻿using System.Collections.Generic;

namespace E.Shop.Store.Data.Models
{
    public class Availability
    {
        public short AvailabilityId { get; set; }
        public string AvailabilityType { get; set; }

        public virtual ICollection<Model> Models { get; set; }
    }
}
