﻿using System.Collections.Generic;

namespace E.Shop.Store.Data.Models
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public int? ImgId { get; set; }

        public virtual ICollection<Model> Models { get; set; }
    }
}
