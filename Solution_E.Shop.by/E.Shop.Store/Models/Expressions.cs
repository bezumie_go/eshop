﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using DynamicExpression = System.Linq.Dynamic.DynamicExpression;

namespace E.Shop.Store.Data.Models
{
    public class Filter<TModel>
    {
        private readonly IList<FilterExpression> _expressionsList;

        public Filter()
        {
            _expressionsList = new List<FilterExpression>();
        }

        public Filter<TModel> AddExpession(FilterExpression expression)
        {
            _expressionsList.Add(expression);
            return this;
        }

        public IEnumerable<FilterExpression> Expressions
        {
            get { return _expressionsList; }
        }

        public Expression<Func<TModel, bool>> GetPredicate()
        {
            var sb = new StringBuilder();
            foreach (var exp in _expressionsList)
            {
                sb.Append(exp.ToExpression(exp.Value));

                if (exp != _expressionsList.Last())
                {
                    sb.Append(exp.Next == ExpressionNext.Or ? " or " : " and ");
                }
            }
            Expression<Func<TModel, bool>> lambda = DynamicExpression.ParseLambda<TModel, bool>(sb.ToString());
            return lambda;
        }

        private StringBuilder GetPredicateStr()
        {
            var sb = new StringBuilder();
            foreach (var exp in _expressionsList)
            {
                sb.Append(exp.ToExpression(exp.Value));

                if (exp != _expressionsList.Last())
                {
                    sb.Append(exp.Next == ExpressionNext.Or ? " or " : " and ");
                }
            }
            return sb;
        }

        public static Expression<Func<TModel, bool>> GetPredicateList(IEnumerable<Filter<TModel>> _filtersExpressionList)
        {
            var sb = new StringBuilder();
            foreach (var exp in _filtersExpressionList)
            {
                if (!exp._expressionsList.Any()) continue;
                if (exp != _filtersExpressionList.First()) sb.Append("and");

                sb.Append("(");
                sb.Append(exp.GetPredicateStr());
                sb.Append(")");
            }
            Expression<Func<TModel, bool>> lambda = DynamicExpression.ParseLambda<TModel, bool>(sb.ToString());
            return lambda;
        }
    }

    public class FilterExpression
    {
        public string Property { get; set; }
        public Condition Condition { get; set; }
        public Type ValueType { get; set; }
        public object Value { get; set; }
        public ExpressionNext? Next { get; set; }

        public FilterExpression(Type valueType)
        {
            ValueType = valueType;
        }

        private static dynamic Cast(dynamic obj, Type castTo)
        {
            return Convert.ChangeType(obj, castTo);
        }

        public string ToExpression(dynamic paramValue)
        {
            var param = Convert.ChangeType(Value, ValueType);
            var exp = string.Empty;
            if (Condition != Condition.Contains)
                exp = $" {Property}{ConditionString(Condition)}{param} ";
            else
            {
                exp = $" {Property}.Contains({paramValue}) ";
            }
            return exp;
        }

        private static string ConditionString(Condition condition)
        {
            string c = string.Empty;
            switch (condition)
            {
                case Condition.Equals:
                    c = "==";
                    break;
                case Condition.GreatOrEqualsThan:
                    c = ">=";
                    break;
                case Condition.LessOrEqualsThan:
                    c = "<=";
                    break;
            }
            return c;
        }
    }

    public enum Condition
    {
        Equals,
        LessOrEqualsThan,
        GreatOrEqualsThan,
        Contains
    }

    public enum ExpressionNext
    {
        And,
        Or
    }
}

