﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using E.Shop.Store.Data.Models;

namespace E.Shop.Store.Data
{
    public interface IGenericRepository<TEntity> : IDisposable where TEntity:class
    {
        IEnumerable<TEntity> Get();
        Task<IEnumerable<TEntity>> GetAsync();
        IEnumerable<TEntity> Get(Func<TEntity, bool> predicate);
        Task<TEntity> FindByIdAsync(int id);
        TEntity FindById(int id);

        IEnumerable<TEntity> Search(Filter<TEntity> searchFilter);
        IEnumerable<TEntity> Search(IEnumerable<Filter<TEntity>> searchFilter);

        void Insert(TEntity item);
        void Remove(int id);
        void Update(TEntity item);
        void Update(TEntity item, TEntity itemNew);
    }
}
