﻿using System.Web.Mvc;

namespace E.Shop.App_Code
{
    public static class HtmlHelpers
    {
        public static string ShortString(this HtmlHelper html, string text, int maxLength)
        {
            if (!string.IsNullOrEmpty(text))
            {
                return text.Length <= maxLength ? text : string.Format($"{text.Substring(0, maxLength)}...");
            }
            return string.Empty;
        }
    }
}