﻿using System;
using System.Collections.Generic;
using System.Web.Configuration;
using System.Web.Mvc;
using E.Shop.Store.Data;
using E.Shop.Story.EF;
using Ninject;
using Ninject.Web.Common;

namespace E.Shop.Util
{
    public class NinjectDependencyResolve : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolve()
        {
            kernel = new StandardKernel();
            AddBindings();
        }

        private void AddBindings()
        {
            kernel.Bind(typeof (IGenericRepository<>)).To(typeof (EFGenericRepository<>))
                .WithConstructorArgument("connectionString",
                    WebConfigurationManager.ConnectionStrings["E.Shop"].ConnectionString);
        }

        public object GetService(Type serviceType)
        {
            //возвращает null, если нет подходящей связки, а не выбрасывает исключение
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            //поддерживает несколько связок для одного типа
            return kernel.GetAll(serviceType);
        }
    }
}