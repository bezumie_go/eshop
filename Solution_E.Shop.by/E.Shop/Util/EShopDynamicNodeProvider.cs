﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using E.Shop.Store.Data;
using E.Shop.Store.Data.Models;
using E.Shop.Story.EF;
using MvcSiteMapProvider;

namespace E.Shop.Util
{
    public class StoreDynamicNodeProvider : DynamicNodeProviderBase
    {
        private IGenericRepository<Category> categories { get; set; }
        private List<DynamicNode> nodes = new List<DynamicNode>();

        public override IEnumerable<DynamicNode> GetDynamicNodeCollection(ISiteMapNode mapnode)
        {
            categories =
                new EFGenericRepository<Category>(WebConfigurationManager.ConnectionStrings["E.Shop"].ConnectionString);

            GetChildrenNodes(null);
            return nodes;
        }

        private void GetChildrenNodes(int? key)
        {
            foreach (var value in categories.Get(p => p.ParentId == key))
            {
                DynamicNode dynamicNode = new DynamicNode
                {
                    Key = value.CategoryId.ToString(),
                    Action = "Index",
                    Controller = "Home",
                    Area = "",
                    Title = value.Name,
                    ParentKey = key.ToString()
                };
                dynamicNode.RouteValues.Add("id", value.CategoryId);
                nodes.Add(dynamicNode);

                if (categories.Get(p => p.ParentId == Convert.ToInt32(value.CategoryId)).Any())
                    GetChildrenNodes(Convert.ToInt32(dynamicNode.Key));
            }
        }
    }
}