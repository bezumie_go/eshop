﻿using System;
using System.Reflection;
using System.Web.Mvc;

namespace E.Shop.Util
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    internal class AcceptAjaxAttribute : ActionMethodSelectorAttribute
    {
        public override bool IsValidForRequest(ControllerContext controllerContext, MethodInfo methodInfo)
        { return controllerContext.HttpContext.Request.IsAjaxRequest(); }
    }

}