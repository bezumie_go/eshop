﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E.Shop.Models
{
        public class ExRateItem
        {
            public string Exchange { get; set; }
            public decimal Rate { get; set; }
        }   
}