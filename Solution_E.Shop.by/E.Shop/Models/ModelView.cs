﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using E.Shop.Store.Data;
using E.Shop.Store.Data.Models;

namespace E.Shop.Models
{
    public class ModelDetailsView : ModelView
    {
        public string Description { get; set; }
        public short Warranty { get; set; }
        public string Availability { get; set; }
        public string Delivery { get; set; }


    }

    public class ModelView
    {
        public int ModelId { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public string imgUrl { get; set; }

    }

    public static class Parse
    {
        static string noImg = "ico_1000000.jpg";

        public static ModelDetailsView ToModelDetailsView(Model model)
        {
            
            var modelDetailsView = new ModelDetailsView
            {
                ModelId = model.ModelId,
                Title = model.Title,
                Price = model.Price,
                imgUrl = ((new FileInfo(HttpContext.Current.Server.MapPath($"~/Content/Images/ico_{model.ImgId}.jpg"))).Exists) ?
                        $"~/Content/Images/ico_{model.ImgId}.jpg"
                        : $"~/Content/Images/{noImg}",
                Warranty = model.Warranty,
                Description = model.Description,
                Delivery = model.Delivery.DeliveryType,
                Availability = model.Availability.AvailabilityType              
            };
          
            return modelDetailsView;
        }


        public static IEnumerable<ModelView> ToModelView(IEnumerable<Model> modelsList)
        {
           // var noImg = "ico_1000000.jpg";
            IEnumerable<ModelView> modelViewsList = modelsList.Select(model => new ModelView
            {
                ModelId = model.ModelId,
                Title = model.Title,
                Price = model.Price,
                imgUrl = ((new FileInfo(HttpContext.Current.Server.MapPath($"~/Content/Images/ico_{model.ImgId}.jpg"))).Exists) ?
                        $"~/Content/Images/ico_{model.ImgId}.jpg"
                        : $"~/Content/Images/{noImg}"
            });

            return modelViewsList;
        }

        public static List<Model> GetModelsByChildrenCategories(int id, IGenericRepository<Model> models, IGenericRepository<Category> category)
        {
            List<Model> modelsList = new List<Model>();

            List<int> categoriesIDList = new List<int>();
            ChildrenCategories(id, categoriesIDList, category);

            foreach (var value in categoriesIDList)
            {
                modelsList.AddRange(models.Get(p => p.CategoryId == value));
            }

            return modelsList;
        }

        private static void ChildrenCategories(int id, List<int> categoriesIDList, IGenericRepository<Category> category)
        {
            IEnumerable<int> childCategoriesID = category.Get(p => p.ParentId == id).Select(i => i.CategoryId);
            categoriesIDList.AddRange(childCategoriesID);
            foreach (var value in childCategoriesID)
            {
                if (category.Get(p => p.ParentId == value).Any())
                {
                    ChildrenCategories(value, categoriesIDList, category);
                }
            }
        }

    }
}