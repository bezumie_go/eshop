﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E.Shop.Models
{
    public class UsersViewModel
    {
        public string Id { get; set; }
        public  string UserName { get; set; }
        public string Email { get; set; }
        public DateTime Data { get; set; }
        public bool ConfirmBan { get; set; }
        public int IsBanHours { get; set; }

        public IEnumerable<string> CurrentRols { get; set; }
    }
}