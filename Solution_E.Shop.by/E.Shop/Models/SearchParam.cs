﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace E.Shop.Models
{
    public class SearchParam
    {
        public int category { get; set; }
        public string searchWords { get; set; }
        public decimal minPrice { get; set; } 
        public decimal maxPrice { get; set; }
        public string availability { get; set; }
    }
}