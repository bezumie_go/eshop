﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using E.Shop.Store.Data;
using E.Shop.Models;
using E.Shop.Store.Data.Models;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using PagedList;

namespace E.Shop.Controllers
{
    public class SearchController : Controller
    {
        private IGenericRepository<Model> models;
        private IGenericRepository<Availability> avail;
        private IGenericRepository<Category> category;
        public int PageSize = 12;
        // GET: Search
        public SearchController(IGenericRepository<Model> models, IGenericRepository<Availability> avail,
            IGenericRepository<Category> category)
        {
            this.models = models;
            this.avail = avail;
            this.category = category;
        }

        // GET: /Search/SearchModels/
        public ActionResult SearchModels()
        {
            List<SelectListItem> items = category.Get(p => p.ParentId == null)
                .Select(
                    p =>
                        new SelectListItem
                        {
                            Text = p.Name.Length > 10 ? $"{p.Name.Substring(0, 10)}..." : p.Name,
                            Value = p.CategoryId.ToString()
                        }).ToList();
            ViewBag.Category = items;
            items = avail.Get()
                .Select(p => new SelectListItem {Text = p.AvailabilityType, Value = p.AvailabilityId.ToString()})
                .ToList();
            ViewBag.Availability = items;
            return PartialView("SearchModels");
        }

        public ActionResult Find(string term)
        {
            var projection = models.Get(p => p.Title.Contains(term)).Select(a => new {value = a.Title}).ToList();
            return Json(projection, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SearchModels(SearchParam paramsModel, int page = 1)
        {
            SearchParam param = paramsModel;
            if (param.category == 0 && string.IsNullOrEmpty(param.availability) && param.maxPrice == 0 &&
                param.minPrice == 0 && string.IsNullOrEmpty(param.searchWords))
            {
                return RedirectToAction("SearchModels");
            }
            else
            {
                List<Filter<Model>> filterList = new List<Filter<Model>>();

                var filter = new Filter<Model>();
                if (param.category != 0)
                {
                    if (models.Get(p => p.CategoryId == param.category).Any())
                        filter.AddExpession(new FilterExpression(typeof (int))
                        {
                            Property = "CategoryId",
                            Condition = Condition.Equals,
                            Value = param.category
                        });
                    else
                    {
                        int id = Convert.ToInt32(param.category);
                        var childrenCatigoriesList =
                            Parse.GetModelsByChildrenCategories(id, models, category)
                                .Select(p => p.CategoryId)
                                .Distinct().ToList();

                        if (childrenCatigoriesList.Any())
                        {
                            filter = new Filter<Model>();
                            var filterCategory = new Filter<Model>();
                            foreach (var value in childrenCatigoriesList)
                            {
                                filterCategory.AddExpession(new FilterExpression(typeof (int))
                                {
                                    Property = "CategoryId",
                                    Condition = Condition.Equals,
                                    Value = value,
                                    Next =
                                        (value != childrenCatigoriesList.Last())
                                            ? ExpressionNext.Or
                                            : ExpressionNext.And
                                });
                            }
                            filterList.Add(filterCategory);
                        }
                    }
                }

                if (!String.IsNullOrEmpty(param.availability))
                    filter.AddExpession(new FilterExpression(typeof (short))
                    {
                        Property = "AvailabilityId",
                        Condition = Condition.Equals,
                        Value = param.availability,
                    });
                if (param.minPrice > 0)
                    filter.AddExpession(new FilterExpression(typeof (decimal))
                    {
                        Property = "Price",
                        Condition = Condition.GreatOrEqualsThan,
                        Value = param.minPrice,
                    });
                if (param.maxPrice > 0)
                    filter.AddExpession(new FilterExpression(typeof (decimal))
                    {
                        Property = "Price",
                        Condition = Condition.LessOrEqualsThan,
                        Value = param.maxPrice,
                    });
                if (!String.IsNullOrEmpty(param.searchWords))
                    filter.AddExpession(new FilterExpression(typeof (string))
                    {
                        Property = "Title",
                        Condition = Condition.Contains,
                        Value = String.Format('"' + param.searchWords + '"'),
                    });
                filterList.Add(filter);

                var json = JsonConvert.SerializeObject(filterList);
                var cookie = new HttpCookie("modelsFilter", json);
                cookie.Expires = DateTime.Now.AddHours(1);
                this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);
                var modelViewList = Parse.ToModelView(models.Search(filterList));
                return PartialView("_ModelViewData", modelViewList.ToPagedList(page, PageSize));
            }
        }

        [ChildActionOnly]
        public ActionResult ModelViewData(PagedList.IPagedList<E.Shop.Models.ModelView> pagedList)
        {
            return PartialView("_ModelViewData", pagedList);
        }

        // GET: /Search/Index/1
        public ActionResult Index(int page = 1)
        {
            IEnumerable<ModelView> modelViewList = null;
            if (Request.Cookies["modelsFilter"] != null)
            {
                HttpCookie cookie = Request.Cookies["modelsFilter"];
                IEnumerable<Filter<Model>> filter =
                    JsonConvert.DeserializeObject<IEnumerable<Filter<Model>>>(cookie.Value);
                modelViewList = Parse.ToModelView(models.Search(filter));
            }

            return PartialView("_ModelViewData", modelViewList.ToPagedList(page, PageSize));
        }
    }
}