﻿using E.Shop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace E.Shop.Controllers
{
    public class ExRatesController : Controller
    {
        // GET: ExRates
        [OutputCache(Duration = 2*3600, VaryByParam = "*")]
        [ChildActionOnly]
        public ActionResult GetExRates()
        {
            var exRateClient = new ExRates.ExRatesSoapClient();
            var exRatesData = exRateClient.ExRatesDaily(DateTime.Now);
            ViewBag.UpdateDate = DateTime.Now;
            var selectedRates = exRatesData.Tables[0].Select(
                "Cur_Abbreviation = 'EUR' OR Cur_Abbreviation='RUB' OR Cur_Abbreviation='USD'").Select(row =>
                    new ExRateItem()
                    {
                        Exchange = row["Cur_Abbreviation"].ToString(),
                        Rate = (decimal) row["Cur_OfficialRate"]
                    }).ToArray();

            /* Использование как тип результата PartialView предотвращает вставку в HTML отклик общей части страницы: 
             * тегов <html><head>..</head><body>…</body>. При использовании PartialView будет выводиться только та HTML разметка, 
             * которая непосредственно генерируется в файле GetExRates.cshtml*/
            return PartialView("GetExRates", selectedRates);
        }

        [OutputCache(Duration = 2*3600, VaryByParam = "*")]
        public ActionResult GetExRatesAjax()
        {
            return GetExRates();
        }
    }
}