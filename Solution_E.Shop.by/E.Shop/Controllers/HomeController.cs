﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using E.Shop.Store.Data;
using E.Shop.Store.Data.Models;
using System.Configuration;
using System.IO;
using System.Web.UI;
using E.Shop.Models;
using Ninject;
using Ninject.Web.Mvc;
using Ninject.Web.Mvc.FilterBindingSyntax;
using E.Shop.Story.EF;
using PagedList;
using E.Shop.Util;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace E.Shop.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        private IGenericRepository<Model> models;
        private IGenericRepository<Category> categories;

        public int PageSize = 12;

        public HomeController(IGenericRepository<Model> models, IGenericRepository<Category> categories)
        {
            this.models = models;
            this.categories = categories;
        }


        // GET: /Home/Index/8119/5
        public async Task<ActionResult> Index(int? id, int page = 1)
        {
            var modelViewList = id == null
                ? Parse.ToModelView(await models.GetAsync())
                : Parse.ToModelView(models.Get(p => p.CategoryId == id).ToList());

            if (!modelViewList.Any())
                modelViewList = Parse.ToModelView(Parse.GetModelsByChildrenCategories((int) id, models, categories));

            var cookie = new HttpCookie("idModel", id.ToString());
            cookie.Expires = DateTime.Now.AddHours(1);
            this.ControllerContext.HttpContext.Response.Cookies.Add(cookie);

            return View(modelViewList.ToPagedList(page, PageSize));
        }

        [AcceptAjax]
        [ActionName("Index")]
        public ActionResult Index_ajax(int page = 1)
        {
            IEnumerable<ModelView> modelViewListAjax = null;

            modelViewListAjax = (!string.IsNullOrEmpty(Request.Cookies["idModel"].Value))
                ? Parse.ToModelView(
                    models.Get(p => p.CategoryId == Convert.ToInt32(Request.Cookies["idModel"].Value)).ToList())
                : Parse.ToModelView(models.Get()).ToList();

            if (!modelViewListAjax.Any())
                modelViewListAjax =
                    Parse.ToModelView(
                        Parse.GetModelsByChildrenCategories(Convert.ToInt32(Request.Cookies["idModel"].Value), models,
                            categories));

            return PartialView("_ModelViewData", modelViewListAjax.ToPagedList(page, PageSize));
        }

        [ChildActionOnly]
        public ActionResult ModelViewData(PagedList.IPagedList<E.Shop.Models.ModelView> pagedList)
        {
            return PartialView("_ModelViewData", pagedList);
        }
    }
}