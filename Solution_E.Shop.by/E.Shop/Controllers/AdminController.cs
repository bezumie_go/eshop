﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using E.Shop.Models;
using E.Shop.Store.Identity.Models;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using E.Shop.Util;
using E.Shop.Store.Identity.Infrastructure;
using Microsoft.AspNet.Identity.Owin;

namespace E.Shop.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private AppUserManager UserManager => HttpContext.GetOwinContext().GetUserManager<AppUserManager>();


        // GET: Admin
        public ActionResult Index()
        {
            var users = UserManager.Users;
            List<UsersViewModel> usersView = new List<UsersViewModel>();
            foreach (var user in users)
            {
                var userView = new UsersViewModel
                {
                    Id = user.Id,
                    Data = user.Data,
                    Email = user.Email,
                    ConfirmBan = user.ConfirmBan,
                    UserName = user.UserName,
                    CurrentRols = UserManager.GetRoles(user.Id)
                };
                usersView.Add(userView);
            }
            return View(usersView);
        }

        [HttpPost]
        public async Task<ActionResult> Delete(string id)
        {
            AppUser user = await UserManager.FindByIdAsync(id);
            if (user != null)
            {
                IdentityResult result = await UserManager.DeleteAsync(user);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    return View("Error", result.Errors);
                }
            }
            else
            {
                return View("Error", new string[] {"User Not Found"});
            }
        }

        public async Task<ActionResult> Edit(string id)
        {
            AppUser user = await UserManager.FindByIdAsync(id);

            if (user != null)
            {
                return View(user);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }


        [HttpPost]
        public async Task<ActionResult> Edit(string id, string email, string password)
        {
            AppUser user = await UserManager.FindByIdAsync(id);
            if (user != null)
            {
                user.Email = email;

                IdentityResult validEmail
                    = await UserManager.UserValidator.ValidateAsync(user);

                if (!validEmail.Succeeded)
                {
                    foreach (string error in validEmail.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                IdentityResult validPass = null;

                if (password != string.Empty)
                {
                    validPass
                        = await UserManager.PasswordValidator.ValidateAsync(password);

                    if (validPass.Succeeded)
                    {
                        user.PasswordHash =
                            UserManager.PasswordHasher.HashPassword(password);
                    }
                    else
                    {
                        foreach (string error in validPass.Errors)
                        {
                            ModelState.AddModelError("", error);
                        }
                    }
                }

                if ((validEmail.Succeeded && validPass == null) || (validEmail.Succeeded
                                                                    && password != string.Empty && validPass.Succeeded))
                {
                    IdentityResult result = await UserManager.UpdateAsync(user);

                    if (result.Succeeded)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        foreach (string error in result.Errors)
                        {
                            ModelState.AddModelError("", error);
                        }
                    }
                }
            }
            else
            {
                ModelState.AddModelError("", "User Not Found");
            }

            return View(user);
        }


        public async Task<ActionResult> Ban(string id)
        {
            if (id == null)
            {
                return View("Error");
            }
            AppUser user = await UserManager.FindByIdAsync(id);
            if (user.ConfirmBan == true && user.Data.AddHours(user.IsBanHours) < DateTime.Now)
            {
                user.ConfirmBan = false;
                user.IsBanHours = 0;
            }

            if (!user.ConfirmBan)
            {
                user.ConfirmBan = true;
                user.IsBanHours = 2;
                user.Data = DateTime.Now;

                IdentityResult result = await UserManager.UpdateAsync(user);

                if (!result.Succeeded)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }
            }
            else
            {
                var time = user.Data.AddHours(user.IsBanHours) - DateTime.Now;
                return View("Error", new string[] {$"User is inactive.  Is Ban Time : {time}"});
            }
            return RedirectToAction("Index");
        }
    }
}