﻿using E.Shop.Store.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using E.Shop.Models;
using E.Shop.Store.Data.Models;

namespace E.Shop.Areas.Admin.Controllers
{
    [Authorize]
    public class ModelsController : Controller
    {
        private IGenericRepository<Model> models;
        private IGenericRepository<Availability> availability;
        private IGenericRepository<Delivery> delivery;

        public ModelsController(IGenericRepository<Model> models, IGenericRepository<Availability> availability,
            IGenericRepository<Delivery> delivery)
        {
            this.models = models;
            this.availability = availability;
            this.delivery = delivery;
        }

        public async Task<ActionResult> Index(int id)
        {
            List<SelectListItem> items =  availability.Get()
                .Select(p => new SelectListItem {Text = p.AvailabilityType}).ToList();
            ViewBag.Availability = items;
            items = delivery.Get()
                .Select(p => new SelectListItem {Text = p.DeliveryType})
                .ToList();
            ViewBag.Delivery = items;
            ModelDetailsView modelDetailsView = Parse.ToModelDetailsView(await models.FindByIdAsync(id));

            return View(modelDetailsView);
        }

        public async Task<ActionResult> Edit(ModelDetailsView modelDetails)
        {
          //  ModelDetailsView modelDetailsView = Parse.ToModelDetailsView();
            Model model = await models.FindByIdAsync(modelDetails.ModelId);
            if (model!= null)
            {

                model.ModelId = modelDetails.ModelId;
                model.Title = modelDetails.Title ?? model.Title;
                model.Description = modelDetails.Description ?? model.Description;
                model.Price = modelDetails.Price;
                model.Warranty = modelDetails.Warranty;
                model.ImgId = model.ImgId;////url
                model.Availability = modelDetails.Availability != null
                    ? availability.Get(p => p.AvailabilityType == modelDetails.Availability).FirstOrDefault()
                    : model.Availability;
                model.Delivery = modelDetails.Delivery != null
                    ? delivery.Get(p => p.DeliveryType == modelDetails.Delivery).FirstOrDefault()
                    : model.Delivery;
                
                models.Update(model);
                return RedirectToAction("Index", model.ModelId);
            }
            return View("Error");
        }

        public ActionResult Delete(int id)
        {
            models.Remove(id);
            return RedirectToAction("Index", "Home");
        }

    }
}