﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using E.Shop.Models;
using E.Shop.Store.Data;
using E.Shop.Store.Data.Models;

namespace E.Shop.Areas.User.Controllers
{
    public class ModelsController : Controller
    {
       
        private IGenericRepository<Model> models;

        public ModelsController(IGenericRepository<Model> models)
        {
            this.models = models;
        }

        // GET: User/Models/Index
        //   [Authorize(Roles = "User")]
        public async Task<ActionResult> Index(int id)
        {
            var users = Request.IsAuthenticated.ToString();
            ModelDetailsView modelDetailsView = Parse.ToModelDetailsView(await models.FindByIdAsync(id));
            return View(modelDetailsView);
        }
   }
}