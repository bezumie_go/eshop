﻿using System.Data.Entity.ModelConfiguration;
using E.Shop.Store.Data.Models;

namespace E.Shop.Story.EF.Mapping
{
    public class AvailabilityConfiguration : EntityTypeConfiguration<Availability>
    {
        public AvailabilityConfiguration()
        {
            ToTable("Availability");
            HasEntitySetName("Availability");
            HasKey(k => k.AvailabilityId);

            Property(p => p.AvailabilityId)
                .IsRequired()
                .HasColumnName("ID")
                .HasColumnType("smallint");

            Property(p => p.AvailabilityType)
                .HasColumnName("AvailabilityType")
                .HasMaxLength(50);

            HasMany(madel => madel.Models).WithRequired(model => model.Availability);

        }
    }
}
