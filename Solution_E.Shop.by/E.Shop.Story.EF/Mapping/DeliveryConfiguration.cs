﻿using System.Data.Entity.ModelConfiguration;
using E.Shop.Store.Data.Models;

namespace E.Shop.Story.EF.Mapping
{
   public class DeliveryConfiguration : EntityTypeConfiguration<Delivery>
    {
       public DeliveryConfiguration()
       {
              ToTable("Delivery");
              HasEntitySetName("Deliveries");
              HasKey(k => k.DeliveryId);

              Property(p => p.DeliveryId)
                  .IsRequired()
                  .HasColumnName("ID")
                  .HasColumnType("smallint");

              Property(p => p.DeliveryType)
                 .HasColumnName("DeliveryType")
                 .HasMaxLength(50);

              HasMany(model => model.Models).WithRequired(model => model.Delivery);
            
        }


    }
}
