﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using E.Shop.Store.Data.Models;

namespace E.Shop.Story.EF.Mapping
{
    public class ModelConfiguration : EntityTypeConfiguration<Model>
    {
        public ModelConfiguration()
        {
            ToTable("Models");
            HasEntitySetName("Models");
            HasKey(k => k.ModelId);

            Property(p => p.ModelId)
                .HasColumnName("ID")
                .HasColumnType("int")
                .IsRequired()
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(p => p.Title)
                .HasMaxLength(512)
                .IsRequired();

            Property(p => p.Description)
                .IsRequired()
                .HasMaxLength(2048);

            Property(p => p.Price)
                .IsRequired()
                .HasPrecision(6, 2);

            Property(p => p.Warranty)
                .IsRequired();

            Property(p => p.ImgId)
                .HasColumnName("ImageID")
                .HasColumnType("int")
                .IsOptional();


            //Foreign key's -------------------------------------
            //.IsRequired() marks that column in the database is NOT NULL
            Property(P => P.CategoryId).IsRequired().HasColumnName("CategoryId").HasColumnType("int");
            Property(p => p.DeliveryId).IsRequired().HasColumnName("Delivery").HasColumnType("smallint");
            Property(p => p.AvailabilityId).HasColumnName("Availability").HasColumnType("smallint");
            //--------------------------------------------------

            //navigation property
            HasRequired(model => model.Availability).WithMany(av => av.Models).HasForeignKey(k => k.AvailabilityId);
            HasRequired(model => model.Delivery).WithMany(delivery => delivery.Models).HasForeignKey(k => k.DeliveryId);
            HasRequired(model => model.Category).WithMany(category => category.Models).HasForeignKey(k => k.CategoryId);
        }
    }
}

