﻿using System.Data.Entity.ModelConfiguration;
using E.Shop.Store.Data.Models;

namespace E.Shop.Story.EF.Mapping
{
    public class CategoryConfiguration : EntityTypeConfiguration<Category>
    {
        public CategoryConfiguration()
        {
            ToTable("Categories");
            HasEntitySetName("Categories");
            HasKey(k => k.CategoryId);

            Property(p => p.CategoryId)
                .IsRequired()
                .HasColumnType("int")
                .HasColumnName("ID");

            Property(p => p.ParentId)
                .IsOptional()
                .HasColumnType("int")
                .HasColumnName("ParentID");

            Property(p => p.ImgId)
                .IsOptional()
                .HasColumnType("int")
                .HasColumnName("ImageID");

            Property(p => p.Name)
                .HasMaxLength(150)
                .HasColumnName("Name")
                .IsRequired();
            

            HasMany(model => model.Models).WithRequired(category => category.Category);
        }
    }
}

