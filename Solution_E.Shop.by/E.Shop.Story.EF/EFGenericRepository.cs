﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using E.Shop.Store.Data;
using E.Shop.Store.Data.Models;

namespace E.Shop.Story.EF
{
   public class EFGenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        private readonly EShopContext _context;
        private DbSet<TEntity> _dbSet;

        public EFGenericRepository(string connectionString)
        {
            this._context = new EShopContext(connectionString);
            _dbSet = _context.Set<TEntity>();
        }

        public async Task<TEntity> FindByIdAsync(int id)
        {
            return await _dbSet.FindAsync(id);
        }
        public TEntity FindById(int id)
        {
            return _dbSet.Find(id);
        }

        public IEnumerable<TEntity> Get()
        {
            return _dbSet.ToList();
        }
        public async Task<IEnumerable<TEntity>> GetAsync()
        {
            return await _dbSet.ToListAsync();
        }

        public IEnumerable<TEntity> Get(Func<TEntity, bool> predicate)
        {
            return _dbSet.AsNoTracking().Where(predicate).ToList();
        }

        public void Update(TEntity item,TEntity itemNew)
        {
            
            _context.Entry(item).CurrentValues.SetValues(itemNew);
            _context.SaveChanges();
        }
        public void Update(TEntity item)
        {
            _context.Entry(item).State = EntityState.Modified;
            _context.SaveChanges();
        }

        public void Insert(TEntity item)
       {
           _dbSet.Add(item);
           _context.SaveChanges();
       }

       public  void Remove(int id)
       {
           _dbSet.Remove(_dbSet.Find(id));
           _context.SaveChangesAsync();
       }

       public IEnumerable<TEntity> Search(IEnumerable<Filter<TEntity>> searchFilter)
       {
           return _dbSet.AsNoTracking().Where(Filter<TEntity>.GetPredicateList(searchFilter)).ToList();
       }

       public IEnumerable<TEntity> Search(Filter<TEntity> searchFilter)
       {
           return _dbSet.AsNoTracking().Where(searchFilter.GetPredicate()).ToList();
       }

       public void Dispose()
       {
           if (_context != null)
           {
               _context.Dispose();
           }
       }

       
    }
}

