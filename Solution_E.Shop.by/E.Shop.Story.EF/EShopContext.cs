﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Reflection;
using E.Shop.Store.Data.Models;
using E.Shop.Story.EF.Mapping;

namespace E.Shop.Story.EF
{
    public class EShopContext  : DbContext
    {
        public EShopContext(string connectionString)
            : base(connectionString)
        {
        }

        public  DbSet<Availability> Availability { get; set; }
        public  DbSet<Delivery> Delivery { get; set; } //доставка
        public  DbSet<Model> Models { get; set; }
        public  DbSet<Category> Categories { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {            
            modelBuilder.Configurations.Add(new AvailabilityConfiguration());
            modelBuilder.Configurations.Add(new DeliveryConfiguration());
            modelBuilder.Configurations.Add(new CategoryConfiguration());
            modelBuilder.Configurations.Add(new ModelConfiguration());
            
            base.OnModelCreating(modelBuilder);
        }
    }
}
